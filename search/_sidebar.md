<!-- docs/_sidebar.md -->

- [< Back to hub](/)
  - [Component](./search/src/component.md)
    - [Classical Component](./search/src/classical-component.md)
    - [Stateless Functional Component](./search/src/stateless-functional-component.md)
    - [Extending Component](./search/src/extending-component.md)
