# Types of Components

There two types of components in Preact:

- [Classical Components](./src/classical-component.md), with [lifecycle methods] and state
- [Stateless Functional Components](./src/stateless-functional-component.md), which are functions that accept `props` and return [JSX].

Within these two types, there are also a few different ways to implement components.
