# Stateless Functional Components

These are just functions that accept `props` as an argument, and return JSX.

```js
const Link = ({ children, ...props }) => <a {...props}>{children}</a>;
```

> _ES2015 Note:_ the above is an Arrow Function, and because we've used parens instead of braces for the function body, the value within the parens is automatically returned. You can read more about this [here](https://github.com/lukehoban/es6features#arrows).
