# Classical Component

## Example

Let's use an example: a simple `<Link>` component that creates an HTML `<a>` element:

```js
class Link extends Component {
  render(props, state) {
    return <a href={props.href}>{props.children}</a>;
  }
}
```

We can instantiate/render this component as follows:

```xml
<Link href="http://example.com">Some Text</Link>
```

### Destructure Props & State

Since this is ES6 / ES2015, we can further simplify our `<Link>` component by mapping keys from `props` (the first argument to `render()`) to local variables using [destructuring](https://github.com/lukehoban/es6features#destructuring):

```js
class Link extends Component {
  render({ href, children }) {
    return <a {...{ href, children }} />;
  }
}
```

If we wanted to copy _all_ of the `props` passed to our `<Link>` component onto the `<a>` element, we can use the [spread operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator):

```js
class Link extends Component {
  render(props) {
    return <a {...props} />;
  }
}
```
